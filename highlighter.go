package liner

import (
	"errors"
	"io"
	"os"
	"strings"
	"unicode"

	"github.com/fatih/color"
)

const (
	COLOR_NOCOLOR = ""
	COLOR_RESET   = "\x1b[0m"
	COLOR_BRIGHT  = "\x1b[1m"

	COLOR_BLACK   = "\x1b[30m"
	COLOR_RED     = "\x1b[31m"
	COLOR_GREEN   = "\x1b[32m"
	COLOR_YELLOW  = "\x1b[33m"
	COLOR_BLUE    = "\x1b[34m"
	COLOR_MAGENTA = "\x1b[35m"
	COLOR_CYAN    = "\x1b[36m"
	COLOR_WHITE   = "\x1b[37m"
)

type Category int

const (
	NumberType   Category = iota //	number. e.g. 10, 10.5
	IdentType                    // identifier. e.g. name, age
	KeywordType                  // keywords.  e.g. if, else
	StringType                   // string. e.g. "hello", 'hello'
	CommentType                  // comment. e.g. //xxxx
	OperatorType                 // operators. e.g. ++, --, +-
	FunctionType                 // identifiers which are known to be functions
)

type Highlighter struct {
	input  []rune //the source we need to highlight
	pos    int
	length int

	keywords  map[string]int //keywords
	functions map[string]int //keywords
	colors    map[Category](*color.Color)

	//operators
	operatorChars string
	operatorArr   []string
}

func NewHighlighter() *Highlighter {
	h := &Highlighter{}
	h.pos = 0
	h.length = 0

	h.colors = make(map[Category](*color.Color))
	h.colors[NumberType] = color.New()
	h.colors[IdentType] = color.New()
	h.colors[OperatorType] = color.New()
	h.colors[KeywordType] = color.New()
	h.colors[StringType] = color.New()
	h.colors[CommentType] = color.New()
	h.colors[FunctionType] = color.New()

	h.keywords = make(map[string]int)
	h.functions = make(map[string]int)

	return h
}

//reset the highlighter for reuse.
func (h *Highlighter) Reset(input []rune) {
	h.input = input
	h.pos = 0
	h.length = len(h.input)
}

func (h *Highlighter) RegisterFunction(id string) {
	h.functions[id] = 1 //map's value is not important
}

func (h *Highlighter) DeregisterFunction(id string) {
	delete(h.functions, id)
}

func (h *Highlighter) RegisterFunctions(functions []string) {
	for _, v := range functions {
		h.functions[v] = 1 //map's value is not important
	}
}

func (h *Highlighter) RegisterKeywords(keywords []string) {
	for _, v := range keywords {
		h.keywords[v] = 1 //map's value is not important
	}
}

func (h *Highlighter) RegisterOperators(operators []string) {
	for _, v := range operators {
		h.operatorArr = append(h.operatorArr, v)
	}
	h.operatorChars = strings.Join(operators, "")
}

func (h *Highlighter) RegisterColors(colors map[Category](*color.Color)) {
	h.colors = colors
}

//Highlight the source code
func (h *Highlighter) Highlight() {
	for h.pos < h.length {
		current := h.peek(0)
		if current == '"' || current == '`' || current == '\'' {
			h.processString(current)
		} else if current == '\\' {
			h.processEscapedNum()
		} else if isLetter(current) {
			h.processIdentifier()
		} else if strings.Contains(h.operatorChars, string(current)) {
			h.processOperator()
		} else if isDigit(current) {
			h.processNumber()
		} else {
			if h.input[h.pos] != '\n' {
				h.processNormal()
			}
			h.next()
		}
	} //end for
}

//process strings(doule quoted string, single quoted string or raw string)
func (h *Highlighter) processString(ch rune) error {
	var ret []rune

	ret = append(ret, ch)

	for {
		if h.next() == 0 {
			goto end
		}
		if h.input[h.pos] == 0 {
			return errors.New("unexpected EOF")
		}

		if h.input[h.pos] == ch {
			if h.next() == 0 {
				ret = append(ret, ch)
				goto end
			}
			break
		}

		ret = append(ret, h.input[h.pos])
	}
	ret = append(ret, ch)

end:
	h.colors[StringType].Print(string(ret))
	return nil
}

func (h *Highlighter) processComment(ch rune) {
	pos := h.pos

	if second := h.next(); second != 0 {
		for h.peek(0) != '\n' && h.peek(0) != 0 {
			if second == '*' && h.peek(0) == '*' && h.peek(1) == '/' {
				h.next()
				h.next()
				break
			}
			h.next()
		}
	}
	text := string(h.input[pos:h.pos])
	h.colors[CommentType].Print(text)
}

//process operator
func (h *Highlighter) processOperator() {
	if h.peek(0) == '/' {
		if c := h.peek(1); c == '/' || c == '*' {
			h.processComment(h.peek(0))
			return
		}
	}

	for _, operator := range h.operatorArr {
		aLen := len(operator)

		var str string
		if h.pos+aLen < h.length {
			str = string(h.input[h.pos : h.pos+aLen])
		} else {
			str = string(h.input[h.pos:])
		}

		if strings.HasPrefix(str, operator) {
			if h.next() == 0 {
				goto end
			}
			if aLen == 2 {
				if h.next() == 0 {
					goto end
				}
			}
		end:
			h.colors[OperatorType].Print(operator)
			break
		} //end if
	} //end for
}

func (h *Highlighter) processEscapedNum() {
	pos := h.pos
	for isLetter(h.input[h.pos]) || isDigit(h.input[h.pos]) || (h.pos == pos) {
		r := h.next()
		if r == 0 {
			break
		}
	}
	text := string(h.input[pos:h.pos])
	h.colors[StringType].Print(text)
}

func (h *Highlighter) processIdentifier() {
	pos := h.pos
	for isLetter(h.input[h.pos]) || isDigit(h.input[h.pos]) {
		r := h.next()
		if r == 0 {
			break
		}
	}

	text := string(h.input[pos:h.pos])

	if text == "true" || text == "false" {
		h.colors[NumberType].Print(text)
	} else if _, ok := h.keywords[text]; ok {
		h.colors[KeywordType].Print(text)
	} else if _, ok := h.functions[text]; ok {
		h.colors[FunctionType].Print(text)
	} else {
		h.colors[IdentType].Print(text)
	}
}

func (h *Highlighter) processNumber() error {
	var ret []rune
	var ch rune = h.input[h.pos]

	ret = append(ret, ch)
	if h.next() == 0 {
		goto end
	}

	if ch == '0' && (h.input[h.pos] == 'x' || h.input[h.pos] == 'b' || h.input[h.pos] == 'c') { //support '0x'(hex) and '0b'(bin) and '0c'(octal)
		savedCh := h.input[h.pos]
		ret = append(ret, h.input[h.pos])
		if h.next() == 0 {
			goto end
		}
		if savedCh == 'x' {
			for isHex(h.input[h.pos]) || h.input[h.pos] == '_' {
				if h.input[h.pos] == '_' {
					ret = append(ret, h.input[h.pos])
					if h.next() == 0 {
						goto end
					}
					continue
				}
				ret = append(ret, h.input[h.pos])
				if h.next() == 0 {
					goto end
				}
			}
		} else if savedCh == 'b' {
			for isBin(h.input[h.pos]) || h.input[h.pos] == '_' {
				if h.input[h.pos] == '_' {
					ret = append(ret, h.input[h.pos])
					if h.next() == 0 {
						goto end
					}
					continue
				}
				ret = append(ret, h.input[h.pos])
				if h.next() == 0 {
					goto end
				}
			}
		} else if savedCh == 'c' {
			for isOct(h.input[h.pos]) || h.input[h.pos] == '_' {
				if h.input[h.pos] == '_' {
					ret = append(ret, h.input[h.pos])
					if h.next() == 0 {
						goto end
					}
					continue
				}
				ret = append(ret, h.input[h.pos])
				if h.next() == 0 {
					goto end
				}
			}
		}
	} else {
		for isDigit(h.input[h.pos]) || h.input[h.pos] == '.' || h.input[h.pos] == '_' {
			if h.input[h.pos] == '_' {
				ret = append(ret, h.input[h.pos])
				if h.next() == 0 {
					goto end
				}
				continue
			}

			if h.input[h.pos] == '.' {
				if h.peek(1) == '.' { //range operator
					goto end
				} else if !isDigit(h.peek(1)) && h.peek(1) != 'e' && h.peek(1) != 'E' { //should be a method calling, e.g. 10.next()
					//Note: there are some limitation about this, i.e. the method name could not begin with 'e' or 'E'
					goto end
				}
			} //end if

			ret = append(ret, h.input[h.pos])
			if h.next() == 0 {
				goto end
			}
		}

		if h.input[h.pos] == 'e' || h.input[h.pos] == 'E' {
			ret = append(ret, h.input[h.pos])
			if h.next() == 0 {
				goto end
			}
			if isDigit(h.input[h.pos]) || h.input[h.pos] == '+' || h.input[h.pos] == '-' {
				ret = append(ret, h.input[h.pos])
				if h.next() == 0 {
					goto end
				}
				for isDigit(h.input[h.pos]) || h.input[h.pos] == '.' || h.input[h.pos] == '_' {
					if h.input[h.pos] == '_' {
						ret = append(ret, h.input[h.pos])
						if h.next() == 0 {
							goto end
						}
						continue
					}
					ret = append(ret, h.input[h.pos])
					if h.next() == 0 {
						goto end
					}
				}
			}
			for isDigit(h.input[h.pos]) || h.input[h.pos] == '.' || h.input[h.pos] == '_' {
				if h.input[h.pos] == '_' {
					ret = append(ret, h.input[h.pos])
					if h.next() == 0 {
						goto end
					}
					continue
				}
				ret = append(ret, h.input[h.pos])
				if h.next() == 0 {
					goto end
				}
			}
		}
	}

end:
	// str := h.colors[NumberType].Sprint(string(ret))
	// io.WriteString(os.Stdout, str)
	// println("string is: " + string(ret))
	h.colors[NumberType].Print(string(ret))
	// h.colors[NumberType].Print(os.Stdout, string(ret))
	return nil
}

func (h *Highlighter) processNormal() {
	io.WriteString(os.Stdout, string(h.input[h.pos]))
}

func isDigit(ch rune) bool {
	return '0' <= ch && ch <= '9' || unicode.IsDigit(ch) || ch == '_'
}

// isHex returns true if the rune is a hex digits.
func isHex(ch rune) bool {
	return ('0' <= ch && ch <= '9') || ('a' <= ch && ch <= 'f') || ('A' <= ch && ch <= 'F')
}

// isBin returns true if the rune is a binary digits.
func isBin(ch rune) bool {
	return ('0' == ch || '1' == ch)
}

// isOct returns true if the rune is a octal digits.
func isOct(ch rune) bool {
	return ('0' <= ch && ch <= '7')
}

func isLetter(ch rune) bool {
	return 'a' <= ch && ch <= 'z' || 'A' <= ch && ch <= 'Z' || ch == '_' || ch == '$' || unicode.IsLetter(ch)
}

func (h *Highlighter) next() rune {
	h.pos++
	return h.peek(0)
}

func (h *Highlighter) peek(relativePos int) rune {
	position := h.pos + relativePos
	if position >= h.length {
		return 0
	}

	return h.input[position]
}
